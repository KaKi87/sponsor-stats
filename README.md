# sponsor-stats

Using SponsorStats, compare SponsorBlock statistics between YouTube channels.

![](https://i.goopics.net/pgp9we.png)

## Features

- Search YouTube channels
- Get stats from one or multiple pages of YouTube videos per channel
- Multilanguage (system value by default)
- Dark mode (system value by default)
- Error reporting (disabled by default), powered by [glitchgit](https://git.kaki87.net/KaKi87/glitchgit)