import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';

import App from './components/App.vue';
import i18nMessages from './assets/i18n.json';

const
    app = createApp(App),
    systemLocale = window.navigator.language.split('-')[0],
    i18n = createI18n({
        locale: Object.keys(i18nMessages).includes(systemLocale) ? systemLocale : 'en',
        messages: i18nMessages
    });

if(process.env.NODE_ENV === 'production'){
    Sentry.init({
        dsn: 'https://3de388fd6c7f829d97385fc4d5294eb3@glitchgit.kaki87.net/4',
        integrations: [new VueIntegration({ Vue: app })]
    });
    Sentry.getCurrentHub().getClient().getOptions().enabled = false;
}

app.use(i18n);
app.mount('#app');
