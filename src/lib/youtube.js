import { retry } from 'ts-retry-promise';
import invidious from 'invidious-api-wrapper';
import axiosTauriAdapter from 'axios-tauri-adapter';

const client = invidious({ axiosAdapter: axiosTauriAdapter });

export const searchChannel = query => retry(
    async () => (await client.search({ query, type: 'channel' })).map(item => ({
        id: item['authorId'],
        name: item['author'],
        thumbnail: 'https:' + item['authorThumbnails'].slice(-1)[0].url,
        videoCount: item['videoCount']
    })),
    { retries: 3 }
);

export const getChannelVideos = (channelId, page) => retry(
    async () => (await client.channel({ channelId }).getVideos({ page })).map(item => ({
        id: item['videoId'],
        title: item['title'],
        thumbnail: item['videoThumbnails'].find(_item => _item['quality'] === 'medium')['url'],
        timestamp: item['published'] * 1000,
        duration: item['lengthSeconds']
    })),
    { retries: 3 }
);