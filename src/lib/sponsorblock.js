import { retry } from 'ts-retry-promise';
import axios from 'axios';
import axiosTauriAdapter from 'axios-tauri-adapter';

const client = axios.create({
    adapter: axiosTauriAdapter,
    validateStatus: code => code === 200 || code === 404
});

export const getVideoSegments = videoId => retry(
    async () => {
        const {
            status,
            data
        } = await client.get(
            `https://sponsor.ajay.app/api/skipSegments`,
            {
                params: {
                    'videoID': videoId,
                    'categories': JSON.stringify(['sponsor', 'selfpromo', 'interaction', 'intro', 'outro', 'preview', 'music_offtopic'])
                }
            }
        );
        return (status === 200 ? data : []).reduce((data, {
            category,
            segment: [
                startTime,
                endTime
            ]
        }) => ({
            ...data,
            [category]: (data[category] || 0) + (endTime - startTime)
        }), {});
    },
    { retries: 3 }
);