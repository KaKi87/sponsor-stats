export default {
    methods: {
        getSetting: key => {
            const data = localStorage.getItem(key);
            try { return JSON.parse(data); }
            catch { return data; }
        },
        setSetting: (key, value) => {
            let data;
            try { data = JSON.stringify(value); }
            catch { data = value; }
            localStorage.setItem(key, data);
        }
    }
}